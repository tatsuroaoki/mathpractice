let answer = document.getElementById("answer")
answer.addEventListener("click",drawCard)

function drawCard() {
	let card1 = Math.floor(Math.random() * 14)
	let card2 = Math.floor(Math.random() * 14)
	let card3 = Math.floor(Math.random() * 14)
	let highestCard = Math.max(card1, card2, card3)
	
	let answer = document.getElementById("answer")
	answer.innerHTML = highestCard
	}

function pizza() {
	// Pizza price
	let pizzaPrice1 = 16.99
	let pizzaPrice2 = 19.99

	// Pizza diameter
	let pizzaDia1 = 13
	let pizzaDia2 = 17
	
	// Pizza surface area
	let pizzaArea1 = Math.PI * (pizzaDia1 / 2) ** 2
	let pizzaArea2 = Math.PI * (pizzaDia2 / 2) ** 2
	
	// Pizza price per square inch
	let costPerSquareInch1 = pizzaPrice1 / pizzaArea1
	let costPerSquareInch2 = pizzaPrice2/  pizzaArea2
	}